extends Area2D

var screensize
export var speed = 100
export var forward_step = 30
var direction

onready var bullet = preload("res://Scenes/EnemyLaser.tscn")

func _ready():
	screensize = get_viewport_rect().size
	direction = "left"
	
func _process(delta):
	var velocity = 0
	
	var outputLaser = randi()%10000
	
	if outputLaser >= 9990:
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y += 30
		get_node("/root/Main/").add_child(n_bullet)
	
	if direction == "left":
		velocity = -1
	else:
		velocity = 1
	
	position.x += velocity * speed * delta
	
	if position.x < -50 or position.x > screensize.x + 50:
		position.y += forward_step
		if direction == "left":
			direction = "right"
		else:
			direction = "left"